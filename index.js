//Bài 1
function sapXep() {
  var so1 = document.getElementById("txt-so1").value * 1;
  var so2 = document.getElementById("txt-so2").value * 1;
  var so3 = document.getElementById("txt-so3").value * 1;

  if (so1 < so2) {
    if (so1 < so3) {
      if (so2 < so3) {
        document.getElementById(
          "result1"
        ).innerText = `${so1} < ${so2} < ${so3}`;
      } else if (so3 < so2) {
        document.getElementById(
          "result1"
        ).innerText = `${so1} < ${so3} < ${so2}`;
      } else {
        document.getElementById(
          "result1"
        ).innerText = `${so1} < ${so2} = ${so3}`; // so 2 = so 3
      }
    } else if (so1 > so3) {
      document.getElementById("result1").innerText = `${so3} < ${so1} < ${so2}`;
    } else {
      document.getElementById("result1").innerText = `${so1} = ${so3} < ${so2}`;
    }
  } else if (so1 > so2) {
    if (so1 > so3) {
      if (so2 > so3) {
        document.getElementById(
          "result1"
        ).innerText = `${so3} < ${so2} < ${so1}`;
      } else if (so2 < so3) {
        document.getElementById(
          "result1"
        ).innerText = `${so2} < ${so3} < ${so1}`;
      } else {
        document.getElementById(
          "result1"
        ).innerText = `${so3} = ${so2} < ${so1}`;
      }
    } else if (so1 < so3) {
      document.getElementById("result1").innerText = `${so2} < ${so1} < ${so3}`;
    }
  } else if (so1 == so2) {
    if (so1 < so3)
      document.getElementById("result1").innerText = `${so1} = ${so2} < ${so3}`;
    else {
      document.getElementById("result1").innerText = `${so1} = ${so2} = ${so3}`;
    }
  }
}
// Bài 2
function guiLoichao() {
  var vlChao = document.getElementById("form-control").value * 1;
  //console.log({ vlChao });
  switch (vlChao) {
    case 1:
      document.getElementById("result2").innerText = "Chào Bố!";
      break;
    case 2:
      document.getElementById("result2").innerText = "Chào Mẹ!";
      break;
    case 3:
      document.getElementById("result2").innerText = "Chào Anh Trai!";
      break;
    case 4:
      document.getElementById("result2").innerText = "Chào Em Gái!";
      break;
  }
}
// Bai 3
function dem() {
  var so1 = document.getElementById("txt-b3so1").value * 1;
  var so2 = document.getElementById("txt-b3so2").value * 1;
  var so3 = document.getElementById("txt-b3so3").value * 1;

  var demChan = 0;

  if (so1 % 2 == 0) {
    demChan++;
  }
  if (so2 % 2 == 0) {
    demChan++;
  }
  if (so3 % 2 == 0) {
    demChan++;
  }

  document.getElementById(
    "result3"
  ).innerText = `Số chẵn có ${demChan} số, Số lẽ có ${3 - demChan} số.`;
}
//Bài 4
function duDoan() {
  var canh1 = document.getElementById("txt-canh1").value * 1;
  var canh2 = document.getElementById("txt-canh2").value * 1;
  var canh3 = document.getElementById("txt-canh3").value * 1;

  if (canh1 == canh2 && canh1 == canh3 && canh2 == canh3) {
    document.getElementById("result4").innerText = "Đây là tam giác đều.";
  } else if (canh1 == canh2 || canh1 == canh3 || canh2 == canh3) {
    document.getElementById("result4").innerText = "Đây là tam giác cân.";
  } else if (
    Math.pow(canh1, 2) == Math.pow(canh2, 2) + Math.pow(canh3, 2) ||
    Math.pow(canh2, 2) == Math.pow(canh1, 2) + Math.pow(canh3, 2) ||
    Math.pow(canh3, 2) == Math.pow(canh2, 2) + Math.pow(canh1, 2)
  ) {
    document.getElementById("result4").innerText = "Đây là tam giác vuông.";
  } else {
    document.getElementById("result4").innerText =
      "Đây là một loại tam giác nào đó.";
  }
}
